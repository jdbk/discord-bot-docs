# Jak si vyrobit primitivního Discord Bota

## Instalace potřebného SW
  * potřebuješ Python a nějaký program na psaní kódu, třeba VS Code

### Python
  * na [webu Pythonu](https://www.python.org/downloads/windows/) si stáhni balík pro widle:
  ![python1](img/python1.png)

  ![python2](img/python2.png)

  * A nainstaluj:

  ![python3](img/python3.png)

### VS Code

  * stání z [webu](https://code.visualstudio.com/)
  ![vscode1](img/vscode1.png)
  * a nainstaluj, bude se to hodit.

## Výroba bota
  
  * spusť VS Code
  * otevři si adresář kde ho budeš vytvářet:
  ![vscode2](img/vscode2.png)

  * vytvoři si soubor, ve kterým bude zdrojový kód, třeba bot.py:
  ![vscode3](img/vscode3.png)
  
  * jakmile ho vytvoříš, nabídne ti to plugin na práci s pythonem:
  ![vscode4](img/vscode4.png)
  
  * nainstaluj:
  * chvíli po kliknutí na install to bude hotově:
  ![vscode5](img/vscode5.png)
  
  
  * běž zpět k souborům, dej terminal - nový terminal:
  ![vscode9](img/vscode9.png)

  * to otevřelo příkazovou řádku dole, v tý nainstalujeme discord plugin do Pythonu - `py -3 -m pip install -U discord.py`
  ![vscode6](img/vscode6.png)

  * teď můžeš vykrást nějaký kód, třeba z [návodu](https://discordpy.readthedocs.io/en/stable/quickstart.html), vložit do něj svůj token, ULOŽIT NA DISK:
  ![vscode7](img/vscode7.png) 
  * a spustit - `py -3 bot.py`
  ![vscode8](img/vscode8.png)


